module.exports = {
    apps: [
        {
            name: 'user-service',
            script: `index.js`,
            node_args: '-r dotenv/config',
            max_memory_restart: '120M',
            interpreter: 'node@18.12.0',
            watch: false,
            autorestart: true
        }
    ]
}
