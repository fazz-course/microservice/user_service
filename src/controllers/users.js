const model = require('../models/users')
const loggs = require('../libs/logger')(module)
const ctrl = {}

ctrl.GetUsers = async (req, res) => {
    try {
        const data = await model.GetAll()
        return res.status(200).send(data)
    } catch (error) {
        loggs.error(error)
        return res.status(500).send(error)
    }
}

ctrl.Update = async (req, res) => {
    try {
        const { user_id } = req.params
        const data = await model.update(user_id, req.body)
        return res.status(200).send(data)
    } catch (error) {
        loggs.error(error)
        return res.status(500).send(error)
    }
}
module.exports = ctrl
