const express = require('express')
const routers = express.Router()
const ctrl = require('../controllers/users')

routers.get('/', ctrl.GetUsers)

module.exports = routers
